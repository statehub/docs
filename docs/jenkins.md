# Cross-Region or Multicloud Jenkins with Statehub

This guide walks you through the process of deploying Jenkins on a Kubernetes cluster, using Statehub for cross-region (or multicloud) business continuity.

**[Statehub](https://statehub.io/) is a managed storage service for Kubernetes, that allows you to establish seamless replication for your persistent volumes between any public cloud regions.**

**What you'll accomplish by following this guide:**

- Deploy an Jenkins on a Kubernetes cluster
- Replicate the database between two cloud regions using Statehub
- Fail over the Jenkins installation to another cluster in the second region
- Generate some data in the database, and demostrate no data loss after failover – all data written in the first region is available on the second region

## Before you begin

Before you begin, it might be useful to familiarize yourself with Statehub concepts such as [clusters](https://docs.statehub.io/clusters.html), [states](https://docs.statehub.io/states.html), [volumes](https://docs.statehub.io/volumes.html), and [the default state](https://docs.statehub.io/states.html#the-default-state).

## Prerequisites

The following is necessary to use follow this guide:

1. A Statehub account. If you don't have a Statehub account, sign up [here](https://statehub.io/start) for the [free tier](subfreetier.md#free-tier).
1. Two Kubernetes clusters, in two different cloud regions. Note that free tier users can only use Statehub at the following regions: `aws/us-west-1`, `aws/us-east-1`, `azure/eastus2`, and `azure/westus2`.
> ℹ️ **Note:** 
> In certain scenarios, you might not have a second cluster in place at another location, but still want your data to be replicated to another location, and create a cluster on-demand in the event of your primary location's failure. In this case, follow the procedure to [add a location to a state](states.md#adding-a-location).
1. A UNIX-compatible command-line interface (Linux or Mac terminal, WSL or CygWin for Windows).
1. The `kubectl` command-line utility to control your clusters.
1. The `helm` command line utility to deploy Helm charts on your clusters.

## Initial Setup
This guide assumes you have two Kubernetes clusters in two distinct cloud locations, similar to the topology below:

## Step 1: Set Up the Statehub CLI

### Get the Statehub CLI

Setting up your Kubernetes clusters to use Statehub requires using the Statehub CLI. After installation is complete, copy the link to your browser and go to the Statehub login page.

Go to [https://get.statehub.io/](https://get.statehub.io/) to Download and Install the Statehub CLI.

Once you've logged in to your Statehub account, you should be automatically redirected to the tokens page and prompted to create a token for your CLI. Click "Yes" to create a token. Copy the token to the CLI prompt and press Enter.

Your Statehub CLI installation should now be configured.

## Step 2: Register Your Clusters with Statehub

In order to use Statehub with your Kubernetes clusters, you must use register them using the Statehub CLI. Register your clusters with Statehub by running the following command:

```bash
statehub register-cluster
```

You'll be able to choose which cluster you want to register from a list populated from your Kubernetes configuration (`KUBECONFIG`). For more information about the cluster registration process, see [Registering a Cluster](clusters.md#registering-a-cluster).


To register another cluster, either switch to the appropriate context and run the aforementioned command. You need to register all the clusters you between which you want to be able to move your stateful applications.

This operation will:

1. Make Statehub aware of your clusters
1. Identify your clusters' location(s) and report them to Statehub
1. Generate a cluster token for your cluster and save it in your cluster's secrets, so that your cluster can access the Statehub REST API.
1. Install the Statehub Controller components and the Statehub CSI driver on your cluster
1. Add the cluster's location(s) to the default state, if default state doesn't span this location yet
1. Configure the default state's corresponding storage class (`default.your-statehub-org-id.statehub.io`) as the default storage class.

Once you've registered all of your clusters, you should be able to start stateful applications and fail them over between your clusters.

## Step 3: configure and apply your cluster components

### Install the Jenkins Helm chart

We will use the [jenkins-operator Helm chart](https://github.com/jenkinsci/helm-charts/tree/main/charts/jenkins).

Add Jenkins to your helm charts repository:

```bash
helm repo add jenkins https://charts.jenkins.io
helm repo update
```

Install the Jenkins chart:

```bash
helm install <RELEASE_NAME> jenkins/jenkins
```

Obtain the newly created admin token that was generated as part of the chart deployment (as part of the secret) by following the instructions in the shell:

> ℹ️ **Note:**
> The Jenkins pod might take a few minutes to turn to "Ready" status.

make sure the jenkins pod is in running state:

```bash
kubectl get pods
```

Obtain the admin user password:

```bash
kubectl exec --namespace default -it svc/'chart-name' -c jenkins -- /bin/cat /run/secrets/chart-admin-password && echo
```

Enter the jenkins console, login with the "admin" user and password:

```bash
echo http://127.0.0.1:8080
kubectl --namespace default port-forward svc/my-jenkins 8080:8080
```

From this point forward you have a functioning Jenkins cluster.

### Generate example pipeline (optional)

In order to demonstrate a switch over, an example of a sample pipeline is built.

Inside the Jenkins console dashboard:

* Add new item
* Choose pipeline and give it a name
* Under pipeline section, try a sample pipeline (i.e. hello world)
* Save

Now return to Dashboard, you will be able to see a record of your build history.

## Step 4: Failing Over Between Clusters in Different Locations

When you need to switch over your application to another cluster, set the other cluster as the owner of the state and apply the configuration to it as follows:


Remove the helm chart from its original location (in this guide the chart name is `my-jenkins`):

```bash
helm delete <my-jenkins>
```

This will delete your jenkins application for the main location cluster.

To start your application on the other cluster

1. Choose the cluster on which you want your application and switch to its context

```bash
kubectl config use-context another-cluster
```

2. Make sure this cluster is the owner of the default state by running the following command:

```bash
statehub set-owner default another-cluster
```

3. Apply your Jenkins application configuration:

```bash
helm install <RELEASE_NAME> jenkins/jenkins
```

Follow instructions (as in step 3) to log in to the Jenkins console, to see the configuration you've set up on the original cluster.

You now have launched a new Jenkins installation in the secondery location with access to the Global PVC that contains the data written on the original cluster.

## Conclusion

You have successfully now deployed an Jenkins installation that's replicated between multiple locations in real time. Statehub makes it resilient to infrastructure failures at the AZ, region and cloud level. Additional locations can be added at any time, so you'll be free to move your applications to any cloud location in the world.
