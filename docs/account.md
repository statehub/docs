In order to use Statehub with your Kubernetes clusters, you must create a Statehub account. This account will allow you to create globally replicated states, so that you can move applications between your different Kubernetes cluster in different cloud vendors or geographical regions.

When you first sign up for a Statehub account, you need to choose your StateHub organization ID. This ID is unique for your organization and cannot be changed.

All resources under your Statehub organization ID are private to your organization, and will only be accessible to your Kubernetes clusters and visible to users from within your organization.

## Signing Up for a Statehub Account

To create a Statehub account:

1. Go to <a href="https://console.statehub.io/auth/register" target="_blank">https://console.statehub.io/auth/register</a>
1. Choose a Statehub organization ID, name, email, and click "Register"
    ![Sign up to Statehub](assets/images/account-signup.png)
1. You'll receive an email requesting you to verify your email address. 

## Access Tokens

Access tokens are used to grant access to the [Statehub REST API](api.md).

The most common use case for REST API access is for using the Statehub CLI. If you want to generate a token for the CLI, it's recommended to follow [this procedure](cli.md#authenticating-the-cli).

To generate an access token:

1. Go to <a href="https://console.statehub.io/tokens/" target="_blank">https://console.statehub.io/tokens/</a>
1. Click the "Generate token" button
1. Enter a name for the token, choose the permissions you want the token to have (all permissions are select by default), and click "Generate token"
1. The token will now be shown to you. Copy it to the your application. It will not be shown again.

    ![Generating a Token](assets/images/account-token.gif)

## Your Profile

Your profile allows you to set personal information like your name, profile picture, email address or password. You can access your profile by going to <a href="https://console.statehub.io/profile/" target="_blank">https://console.statehub.io/profile/</a>.

<!-- Add a section about managing the organizaion and members -->

## Subscription

When you first create your Statehub account, your account is in the [Free Tier](subfreetier.md#free-tier). To remove the Free Tier limitations, you must upgrade your account to a [Pay as You Go Subscription](subfreetier.md#pay-as-you-go-subscription).

To upgrade your subscription to a paid tier:

1. Go to <a href="https://console.statehub.io/subscription/" target="_blank">https://console.statehub.io/subscription/</a>
2. Choose the marketplace through which you want to subscribe
3. Follow the instructions of the marketplace to create a subscription, until you will be redirected back to Statehub.
