Welcome to Statehub!

Statehub is a stateful application mobility service for Kubernetes. It allows you to move your stateful applications between different Kubernetes clusters immediately, regardless of where they are in the public cloud.

Before you sign up for Statehub, it is important to make sure Statehub meets the demands of your operational requirements and DevOps environment. Please review the following characteristics of Statehub, and check them against your needs.

## Function and Purpose: Stateful Application Mobility

Statehub is a managed data service designed to facilitate stateful application mobility between Kubernetes clusters, regardless of their respective locations in the public cloud. Since the vast majority of business applications rely on persistent state, whether stored on persistent volumes attached to the cluster or somewhere else (e.g., a first-party cloud DB service like Amazon RDS, etc.).

**Stateful application mobility is a technological ability required for the achievement of the following goals**:

1. **Ensuring resiliency, availability, and uptime**<br/>
   When an infrastructure failure occurs, the quickest way to continue your operation is to resume it in a location that is not affected by the failure. This requires your applications' state to be synchronized between different locations.
2. **Operational flexibility and cost optimization**<br/>
   Market conditions are often unpredictable. It may be necessary to move your operation or parts of it to a different cloud vendor or geographical location in response to customer or regulatory demand.
   On top of that, different cloud vendors set different prices for their services, sometimes even varying by region. Being able to leverage the competition between the cloud vendors and the differences in prices can save your business money.
3. **Neutralizing data gravity and vendor lock-in**<br/>
   Moving your operation between cloud regions is difficult. Moving your operation between cloud vendors is nigh impossible. The reason for this is data gravity, an observed phenomenon in which the more data your operation has accrued, the more costly and time consuming it is to move. As a consequence of that, you end up setting up all your applications in the same location where all your data is.
   The ability to move stateful applications between locations, allows you to be free of these concerns, and choose your cloud vendor and region according to your needs, instead of forever staying you have been operating historically.

## A Service for Kubernetes

Statehub is purposly built for, and supports only Kubernetes workloads.

Application mobility stands on three pillars: (1) **configuration mobility**, (2) **code mobility**, and (3) **state mobility**. The first two are taken care of by the Kubernetes ecosystem, while the third, state mobility, requires a complementing solution.

1. **Configuration mobility**<br/>
   In order to be able to start your applications anywhere, you need to be able to have the complete configuration of your Ops environment available to from anywhere in the world, and a mechanism that interprets the configuration and deploys your applications accordingly.
   Kubernetes takes care of this by having a declarative configuration model, an infrastructure-as-code (IaC) approach, and an ecosystem of services for persisting this configuration (i.e., GitHub).
2. **Code mobility**<br/>
   In order to be able to start your applications anywhere, you need to be able to deliver the application code to where it needs run, regardless of the of the location, and a mechanism for maintaining the runtime and lifecycle of your code.
   Kubernetes takes care of this by using containers, and deploying container images that are stored on a globally available container registry (i.e., Docker Hub).
3. **State mobility**<br/>
   In order to be able to start your applications anywhere, you need to be able to have your data available for your applications wherever and whenever you start them. Your applications might rely on terabytes or even petabytes of data without which your business cannot operate.
   Kubernetes does not take care of state mobility since it requires vast infrastructure that's outside of the cluster. However, it allows for extensibility when it comes to data and storage infrastructure, allowing Statehub to be the infrastructure that facilitates state mobility.

Kubernetes is the most widely adopted cloud-native application development, deployment and orchestration technology. Statehub builds upon Kubernetes to enable application mobility.

## Managed Service in the Public Cloud

Statehub is offered only in the public cloud. Currently the supported cloud vendors are:

1. Amazon Web Services (AWS)
2. Microsoft Azure

Support is planned for Google Cloud Platform and other vendors in the near future.

Statehub provides a Kubernetes interface that automatically manages the global mobility of applications' state. By virtue of being a fully managed service, Statehub takes full care of the following technical aspects involved:

1. **Cross-cloud and cross-region networking**<br/>
   Statehub operates a global data mobility network called the Global Data Fabric. It spans all the regions of the cloud vendors supported by Statehub. This means you do not have to set up, configure, maintain and monitor the network connections between your cloud regions or cloud vendors. It also means **you don't pay the egress charges levied by your cloud vendor** when your application state is being replicated between your locations.
2. **Data and Storage Management**<br/>
   When your applications consume persistent volumes from Statehub, the data is stored in the same cloud regions as your Kubernetes are, albeit on Statehub's accounts. You do not need to manage capacity, cloud storage, or its attachment to your Kubernetes nodes. It is all taken care of transparently by the Statehub service.
3. **Security**<br/>
   All data stored on Statehub is encrypted. It's synchronized between the different locations using Statehub's private network that spans all public cloud regions, without ever going through the internet. The communication between your clusters and Statehub is also private, since it's using the clouds vendors' Private Link offering (on AWS and Azure).
4. **Infrastructure-level replication**<br/>
   Statehub replicated the data between your locations in real time, on the block level. That means that no configuration is required on the application level for it to be resilient and movable between different regions or clouds. The data just appears wherever you might need to use it, in a seamless manner to your applications and databases.
5. **Infrastructure as Code**<br/>
   Statehub maintains uniformity of your declarative configuration between clusters. When you want to move your application between two cluster, even if they're in a different region or cloud provider, you can simply to apply the same YAML file to the target cluster, without any change. This is possible because Statehub maintains a global naming scheme, and teaches your cluster how to access the nearest instance of your application state, regardless of where it is in the world.

These abilities make Statehub quick to set up. An application mobility project that could take weeks or months to plan an execute can now be completed in minutes.
