# Cross-Region or Multicloud InfluxDB with Statehub

This guide walks you through the process of deploying InfluxDB on a Kubernetes cluster, using Statehub for cross-region (or multicloud) business continuity.

**[Statehub](https://statehub.io/) is a managed storage service for Kubernetes, that allows you to establish seamless replication for your persistent volumes between any public cloud regions.**

**What you'll accomplish by following this guide:**

- Deploy an InfluxDB database on a Kubernetes cluster
- Replicate the database between two cloud regions using Statehub
- Fail over the InfluxDB installation to another cluster in the second region
- Generate some data in the database, and demostrate no data loss after failover – all data written in the first region is available on the second region

![full architecture](assets/images/influxdb-guide/img1.png)

## Before you begin

Before you begin, it might be useful to familiarize yourself with Statehub concepts such as [clusters](https://docs.statehub.io/clusters.html), [states](https://docs.statehub.io/states.htm), [volumes](https://docs.statehub.io/volumes.html), and [the default state](https://docs.statehub.io/states.html#the-default-state).

## Prerequisites

The following is necessary to use follow this guide:

1. A Statehub account. If you don't have a Statehub account, sign up [here](https://statehub.io/start) for the [free tier](subfreetier.md#free-tier).
1. Two Kubernetes clusters, in two different cloud regions. Note that free tier users can only use Statehub at the following regions: `aws/us-west-1`, `aws/us-east-1`, `azure/eastus2`, and `azure/westus2`.
> ℹ️ **Note:** 
> In certain scenarios, you might not have a second cluster in place at another location, but still want your data to be replicated to another location, and create a cluster on-demand in the event of your primary location's failure. In this case, follow the procedure to [add a location to a state](states.md#adding-a-location).
1. A UNIX-compatible command-line interface (Linux or Mac terminal, WSL or CygWin for Windows).
1. The `kubectl` command-line utility to control your clusters.
1. The `helm` command line utility to deploy Helm charts on your clusters.


## Initial Setup

This guide assumes you have two Kubernetes clusters in two distinct cloud locations, similar to the topology below:

![initial setup](assets/images/influxdb-guide/img2.png)

## Step 1: Set Up the Statehub CLI

### Get the Statehub CLI

Setting up your Kubernetes clusters to use Statehub requires using the Statehub CLI. After installation is complete, copy the link to your browser and go to the Statehub login page.

Go to [https://get.statehub.io/](https://get.statehub.io/) to Download and Install the Statehub CLI.

Once you've logged in to your Statehub account, you should be automatically redirected to the tokens page and prompted to create a token for your CLI. Click "Yes" to create a token. Copy the token to the CLI prompt and press Enter.

Your Statehub CLI installation should now be configured.

## Step 2: Register Your Clusters with Statehub

In order to use Statehub with your Kubernetes clusters, you must use register them using the Statehub CLI. Register your clusters with Statehub by running the following command:

```bash
statehub register-cluster
```

You'll be able to choose which cluster you want to register from a list populated from your Kubernetes configuration (`KUBECONFIG`). For more information about the cluster registration process, see [Registering a Cluster](clusters.md#registering-a-cluster).


To register another cluster, either switch to the appropriate context and run the aforementioned command. You need to register all the clusters you between which you want to be able to move your stateful applications.

This operation will:

1. Make Statehub aware of your clusters
1. Identify your clusters' location(s) and report them to Statehub
1. Generate a cluster token for your cluster and save it in your cluster's secrets, so that your cluster can access the Statehub REST API.
1. Install the Statehub Controller components and the Statehub CSI driver on your cluster
1. Add the cluster's location(s) to the default state, if default state doesn't span this location yet
1. Configure the default state's corresponding storage class (`default.your-statehub-org-id.statehub.io`) as the default storage class.

Once you've registered all of your clusters, you should be able to start stateful applications and fail them over between your clusters.

At this point, your setup will look like this:

![statehub registered](assets/images/influxdb-guide/img3.png)

## Step 3: Install the InfluxDB Helm Chart

In this example:

- We'll be using the [`bitnami/influxdb`](https://hub.kubeapps.com/charts/bitnami/influxdb) Helm chart.
- The architecture of the influxdb will be set to use `High-availability` for deploying the DB with a StatefulSet (STS) and a Persistent Volume Claim (PVC) that will be used as part of the cross-region [state](states.md) on Statehub.
- The chart name is `my-release`.
- The AdminPassowrd is predefined and will be stored in a secret.

Add `bitnami` to your helm charts repository:

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
```

Install the `bitnami/influxdb` chart:

```bash
helm install <my-release> bitnami/influxdb --set architecture=high-availability --set auth.admin.password=MyAdminPassword
```

Obtain the admin token that was generated as part of the chart deployment:

```bash
kubectl get secret
```

Extract the token from the secret (using the secret name), and save the admin user token for later use:

```bash
kubectl get secret <my-release-influxdb> -o jsonpath="{.data.admin-user-token}" | base64 -d
```

**At this point, you have an InfluxDB installation that's replicated between your locations.**

## Step 4: Generate Test Data (Optional)

Once the influxdb pod turns to 'Running' state you can launch a pod with the admin user credentials (make sure you change the secret name accordingly):

```bash
kubectl run <my-release-influxdb-client> --rm --tty -i --restart='Never' --namespace default --env INFLUX_TOKEN=$(kubectl get secret my-release-influxdb -o jsonpath="{.data.admin-user-token}" | base64 -d) \
        --image docker.io/bitnami/influxdb:2.1.1-debian-10-r50 \
        --command -- bash
```

Inside the command prompt, write to the DB any sort of data you wish, for example:

```bash
influx write --host http://my-release-influxdb:8086 -o primary -b primary -p s  'statehub,host=us-east-1 owner="true" 1556896326'
```

```bash
influx write --host http://my-release-influxdb:8086 -o primary -b primary -p s  'statehub,host=us-east-2 owner="true" 1556896336'
```

```bash
influx write --host http://my-release-influxdb:8086 -o primary -b primary -p s  'statehub,host=us-west-2 owner="true" 1556496326'
```

In order to read the data, you can use a query method with a time range that fit your messages, for example:

```bash
influx query --host http://my-release-influxdb:8086 -o primary 'from(bucket:"primary")  |> range(start:-10y)'
```

You now have new data ready to be tested for a switch over between locations using Statehub.

After starting your application, your topology will resemble this:

![active location](assets/images/influxdb-guide/img4.png)

## Step 5: Failing Over Between Clusters in Different Locations

![failover](assets/images/influxdb-guide/img5.png)

> ℹ️ **Note:** 
>
>  Before removing the application from the originaly location make sure you have saved the Admin Token and password as mentioned in step 3.
Both of these arguments will be needed for acssees to the data from the new location

When you need to switch over your application to another cluster, set the other cluster as the [owner of the state](states.md#state-ownership) and apply the configuration to it as follows:

Remove the helm chart from its original location (optional, so it will be easier for you to fail the application back):

```bash
helm delete <my-release>
```

### Start Your Application on the Other Cluster:

1. Choose the cluster on which you want your application and switch to its context

        kubectl config use-context another-cluster

1. Make sure this cluster is the owner of the default state by running the following command:

        statehub set-owner default another-cluster

1. Apply your stateful application configuration using the token obatined before at step 3:

        helm install my-release bitnami/influxdb \
                --set architecture=high-availability \
                --set auth.admin.password=MyAdminPassword \
                --set auth.admin.token=IY4kqPdYM9AkfRMNlQWW

### Query the DB for the Data That Was Written in the Original Location:

1. Launch a client pod:

        kubectl run <my-release-influxdb-client> --rm --tty -i --restart='Never' \
        --namespace default \
        --env INFLUX_TOKEN=$(kubectl get secret my-release-influxdb -o jsonpath="{.data.admin-user-token}" | base64 -d) \
                --image docker.io/bitnami/influxdb:2.1.1-debian-10-r50 \
                --command -- bash

1. At the command prompt:

        influx query --host http://my-release-influxdb:8086 -o primary 'from(bucket:"primary")  |> range(start:-10y)'

You now have launched a new InfluxDB cluster in the secondery location with access to the Global PVC that contains the data written on the original cluster.

![after failover](assets/images/influxdb-guide/img6.png)

## Conclusion

You have successfully now deployed an InfluxDB installation that's replicated between multiple locations in real time. Statehub makes it resilient to infrastructure failures at the AZ, region and cloud level. Additional locations can be added at any time, so you'll be free to move your applications to any cloud location in the world.
