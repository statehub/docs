When you sign up to Statehub, you're automatically subscribed to the Statehub Free Tier – a functional Statehub subscription with limited resource consumption that lets you try the Statehub functionality before committing to a paid subscription.

## Free Tier

The Free Tier is the default subscription tier that lets you start working with Statehub right away when signing up for a Statehub account.

> ** ⚠️ Warning:**
>
> Although the Free Tier is perpetual (not time-limited), it is not advisable for use with production workloads, since support will not be provided by Statehub.

The following are the Free Tier limitations:

- **Number of states** is limited to **1**. Upon signup, the [default state](states.md#the-default-state) is created, so this limit is reached immediately. While you can delete the default state and create another one, Statehub recommends to conduct your evaluation and testing on the default state.
- **Number of volumes per state** is limited to **5**.
- **Total size of all volumes** is limited to **100GiB**.
- **Number of locations per state** is limited to **2**.
- **Allowed locations** are only **`aws/us-west-1`, `aws/us-east-1`, `azure/eastus2`, `azure/westus2`**. This means that only clusters that are installed in these locations will be able to connect to your states as long as your account is in Free Tier.

> **ℹ️ Note:**
>
> If you want to evaluate Statehub for your project, but unable to do so within the Free Tier limits, you can request a temporary increase of the limits (e.g., different locations, or larger volumes) by [contacting sales](mailto:sales@statehub.io?subject=Free Tier Limit Increase), specifying your organization ID, use case, and required changes to the limits.

## Pay as You Go Subscription

To remove the limitations on your Statehub account, you have to upgrade it to a paid subscription. A paid Statehub subscription is available through the cloud marketplaces. You can check the Statehub pricing, and subscribe at:

- [AWS Marketplace](http://aws.amazon.com/marketplace/pp/B09CGW4C5F)
- [Azure Marketplace](https://azuremarketplace.microsoft.com/en-us/marketplace/apps/replix1589805524910.statehub-2021-08?tab=Overview)

Please follow the subscription procedure as described [here](account.md#subscription).

You only need to subscribe **once** through **one of the marketplaces**, even though your Statehub states might have locations in multiple clouds. If you're using Statehub as part of a migration scenario, it is recommended you subscribe through the marketplace of the cloud you're migrating *to*.

While a Pay as You Go subscription grants your account an unlimited use of Statehub, a set of limitations is imposed by default to prevent accidental provisioning of a large number of resources, to avoid unexpected excessive billing.

The following are the default limitations:

- **Number of states** is limited to **10**.
- **Number of volumes per state** is limited to **10**.
- **Total size of all volumes** is limited to **2000GiB**.
- **Number of locations per state** is limited to **4**.
- **Allowed locations** are **not limited**.

> **ℹ️ Note:**
>
> You can request an increase to these limits at any time by [contacting support](mailto:support@statehub.io?subject=Request Limit Increase), specifying your organization ID, and the required changes to the limits. The request will be typically handled within a few hours.

### Unsubscribing

You can end your Pay as You Go subscription at any time, by unsubscribing from the dashboard of the cloud through the marketplace of which you've initially subscribed.

When you unsubscribe:

1. Your account limits are reset to Free Tier
1. If your provisioned Statehub resources exceed the Free Tier limits:
    1. **After 24 hours** of unsubscribing, your clusters will no longer be able to access the volumes of the states visible to them. Data will not be affected.
    1. **After 30 days** of unsubscribing, **all your data will be deleted** and the account will reset to it's initial form – an empty default state.

Accounts that fit within the limitations of the Free Tier won't be affected. If you have reduced your Statehub resource consumption after unsubscribing to fit the limitations of Free Tier, service will resume.

You can resubscribe at any time, to resume normal operation.

> **ℹ️ Note:**
>
> If you want to change the marketplace through which you are billed, you have 24 hours to perform a subscription through the new marketplace after you unsubscribe from your existing subscription without your service being affected.
