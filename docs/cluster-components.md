This page goes into the details of the mechanisms and cluster-side components used by Statehub to achieve its functionality.

Statehub installs the following components on registered clusters:

1. **statehub-controller**

2. **state-controller**

3. **statehub-csi-controllers**

Each controller has a different role in the flow of providing the workload (StatefulSet, etc.) with its volumes. This document describes the different controllers' operation and responsibilities.

## From Registering the Cluster to Using a Volume

1. **The `statehub-system` namespace is created**

    The CLI creates the `statehub-system` namespace (or another namespace if [overridden](clusters.md#choose-k8s-namespace-to-install-statehub)) that will contain the controllers and the various secrets and configuration items needed for their operation.
    
    ![step 1](assets/images/frontend-flow-guide/step1.png)

1. **The cluster token**

    The Statehub CLI creates a *cluster token* - a dedicated token used by the specific cluster to authenticate and identify itself with the Statehub API. The cluster token is stored within a secret called **`statehub-cluster-token`** in the **`statehub-system`** namespace, which is later used by all of Statehub’s custom controllers.
    
    The CLI also stores the [cloud credentials you provide](clusters.md#provide-cloud-credentials) in the secrets in the same namespace.

    ![step 2](assets/images/frontend-flow-guide/step2.png)


1. **Component installation** 

    The CLI uses `helm` to install **statehub-controller**, **state-controller**, and **statehub-csi-controller**, which consist of multiple deployments, and a daemonset that's running **statehub-csi-node** on each node.

    ![step 3](assets/images/frontend-flow-guide/step3.png)

1. **Discovery of topology**

    On startup, the **statehub-csi-node** discovers its node's VPC, subnet and availability zone and adds the corresponding topology labels to the node, e.g. `topology.statehub.csi/vpc`.

    ![step 4](assets/images/frontend-flow-guide/step4.png)


1. Creation of the State resources

    **statehub-controller** creates a State [custom resources](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/) representing each of the customer's states that's available in the cluster's location(s). Internally, the controller uses the [Statehub/openapi-go](https://gitlab.com/statehub/openapi-go) library to get information about the states from the Statehub API using the cluster token.

    ![step 5](assets/images/frontend-flow-guide/step5.png)

    >**ℹ️ Note:**
    >
    >Using the cluster token, the API will only return states which the cluster is supposed to be able to use.

    In cases a state is marked as deleted, it sends a delete request to the Kubernetes API and the server sets the object's `metadata.deletionTimestamp` field and the `metadata.finalizers` field to `foregroundDeletion`.The finalizer which we will discuss later, takes care of deleting all the external resources, and the object remains visible through the Kubernetes API until the deletion process is complete. 

1. Creation of Private Link / Private Service Connect endpoints

    The **state-controller** (which is developed using the [Operator Framework SDK](https://sdk.operatorframework.io/)) watches resources of the `states.state.statehub.io` type for update, create or delete events and triggers the reconcile loop.

    The reconcile loop ensures that for any given state, the actual status of both the resource representing the state inside the cluster and the status of the external resources the state needs to function matches the desired status.

    In order to connect the customers' workloads to the to their volumes, Statehub uses Private Link on AWS and Azure or Private Service Connect on GCP, a cloud solution that secures the connection between customers and 3rd party services in different cloud accounts by creating private endpoints in the customers account. For more informationn on Private Link and Private Serivce Connect, go to the respective cloud vendor's documentation at the following links: [Azure Private Link](https://docs.microsoft.com/en-us/azure/private-link/), [AWS Private Link](https://docs.aws.amazon.com/vpc/latest/userguide/endpoint-services-overview.html), [GCP Private Service Connect](https://cloud.google.com/vpc/docs/private-service-connect).

    The **state-controller** creates a private link endpoint in each of the nodes’ subnets using the service ID which it gets from the Statehub API as part of the `GET /states` response and updates the state object's status with the endpoints' IDs, their IPs, security groups IDs and status.

    For every endpoint that reaches the "Ready" status, the **state-controller** adds the endpoint's subnet topology labels to the storage classes' `allowedTopologies` list, and by that allowing volumes of that storage class to be provisioned on the nodes that have the same `topology.statehub.csi/subnet` label value.

    ![step 6](assets/images/frontend-flow-guide/step6.png)

1. Creating the PersistentVolumeClaim

    The [Container Storage Interface](https://github.com/container-storage-interface/spec/blob/master/spec.md) (CSI) defines a standard interface for container orchestration systems (like Kubernetes) to expose arbitrary storage systems to their container workloads.

    Using CSI, Statehub exposes its global storage layer in the customer’s Kubernetes cluster. The Statehub CSI allows users to create PersistentVolumeClaims or volumeClaimTemplates for their Kubernetes workload, by specifying the desired state's [corresponding storage class](states.md#visibility-and-access) in the claim's `storageClassName` field.
    
    As mentioned earlier, the Statehub CSI consists of two components, the **statehub-csi-controller** and the **statehub-csi-node**, each playing a different role in this flow. **statehub-csi-controller** is responsible for listing, creating, and deleting Statehub volumes from the Statehub API. **statehub-csi-node**, which is deployed on every node in the cluster with a DaemonSet, takes care of node-specific processes like connecting the node to the Statehub data plane and mounting it so that a pod deployed on the node will be able to access the volumes it requires. 

    ![step 7](assets/images/frontend-flow-guide/step7.png)

1. The Statehub volume is created

    When a workload claiming a Statehub volume is deployed, the Kubernetes scheduler takes the `storageCalss allowedTopologies` under consideration and assigns the pod to a node. Once the pod is deployed on a node, and if the cluster is the state's [owner](states.md#state-ownership), the **statehub-csi-controller** creates a Statehub volume in the specified state if it doesn’t already exists.

    ![step 8](assets/images/frontend-flow-guide/step8.png)

1. Making the volume available to the pod

    Once the volume is ready, and if the cluster is the state's [owner](states.md#state-ownership), the **statehub-csi-node** it sets the [volume's active location](volumes.md#volumes-active-location) to the node's location (in case the cluster is spread across multiple locations), connects the node to the private link endpoint created earlier, mounts the device on the host node, and then to the specific pod that claimed the volume.

    ![step 9](assets/images/frontend-flow-guide/step9.png)
