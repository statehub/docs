# Cross-Region or Multicloud MinIO with Statehub

This guide walks you through the process of deploying MinIO on a Kubernetes cluster, using Statehub for cross-region (or multicloud) business continuity.

**[Statehub](https://statehub.io/) is a managed storage service for Kubernetes, that allows you to establish seamless replication for your persistent volumes between any public cloud regions.**

**What you'll accomplish by following this guide:**

- Deploy a MinIO database on a Kubernetes cluster in standalone mode
- Replicate the database between two cloud regions using Statehub
- Fail over the MinIO installation to another cluster in the second region
- Generate some data in the database, and demostrate no data loss after failover – all data written in the first region is available on the second region

![full architecture](assets/images/minio/img1.png)

## Before you begin

Before you begin, it might be useful to familiarize yourself with Statehub concepts such as [clusters](https://docs.statehub.io/clusters.html), [states](https://docs.statehub.io/states.htm), [volumes](https://docs.statehub.io/volumes.html), and [the default state](https://docs.statehub.io/states.html#the-default-state).

## Prerequisites

The following is necessary to use follow this guide:

1. A Statehub account. If you don't have a Statehub account, sign up [here](https://statehub.io/start) for the [free tier](subfreetier.md#free-tier).
1. Two Kubernetes clusters, in two different cloud regions. Note that free tier users can only use Statehub at the following regions: `aws/us-west-1`, `aws/us-east-1`, `azure/eastus2`, and `azure/westus2`.
> ℹ️ **Note:** 
> In certain scenarios, you might not have a second cluster in place at another location, but still want your data to be replicated to another location, and create a cluster on-demand in the event of your primary location's failure. In this case, follow the procedure to [add a location to a state](states.md#adding-a-location).
1. A UNIX-compatible command-line interface (Linux or Mac terminal, WSL or CygWin for Windows).
1. The `kubectl` command-line utility to control your clusters.
1. The `helm` command line utility to deploy Helm charts on your clusters.


## Initial Setup

This guide assumes you have two Kubernetes clusters in two distinct cloud locations, similar to the topology below:

![initial setup](assets/images/influxdb-guide/img2.png)

## Step 1: Set Up the Statehub CLI

### Get the Statehub CLI

Setting up your Kubernetes clusters to use Statehub requires using the Statehub CLI. After installation is complete, copy the link to your browser and go to the Statehub login page.

Go to [https://get.statehub.io/](https://get.statehub.io/) to Download and Install the Statehub CLI.

Once you've logged in to your Statehub account, you should be automatically redirected to the tokens page and prompted to create a token for your CLI. Click "Yes" to create a token. Copy the token to the CLI prompt and press Enter.

Your Statehub CLI installation should now be configured.

## Step 2: Register Your Clusters with Statehub

In order to use Statehub with your Kubernetes clusters, you must use register them using the Statehub CLI. Register your clusters with Statehub by running the following command:

```bash
statehub register-cluster
```

You'll be able to choose which cluster you want to register from a list populated from your Kubernetes configuration (`KUBECONFIG`). For more information about the cluster registration process, see [Registering a Cluster](clusters.md#registering-a-cluster).


To register another cluster, either switch to the appropriate context and run the aforementioned command. You need to register all the clusters you between which you want to be able to move your stateful applications.

This operation will:

1. Make Statehub aware of your clusters
1. Identify your clusters' location(s) and report them to Statehub
1. Generate a cluster token for your cluster and save it in your cluster's secrets, so that your cluster can access the Statehub REST API.
1. Install the Statehub Controller components and the Statehub CSI driver on your cluster
1. Add the cluster's location(s) to the default state, if default state doesn't span this location yet
1. Configure the default state's corresponding storage class (`default.your-statehub-org-id.statehub.io`) as the default storage class.

Once you've registered all of your clusters, you should be able to start stateful applications and fail them over between your clusters.

At this point, your setup will look like this:

![statehub registered](assets/images/influxdb-guide/img3.png)


## Step 3: Install the MinIO Operator Helm Chart

In this example:

- We'll be using the [`minio/operator`](https://github.com/minio/operator/tree/master/helm/operator) Helm chart.
- The architecture will be in `Standalone` mode for deploying a single Tenant, 
> ℹ️ **Note:** 
> You may adjust the chart configuration in case you wish to deploy the architecture in `Distributed` mode.
For further information you [click here](https://github.com/minio/operator/blob/master/helm/tenant/values.yaml).

- The chart name is `minio-operator`.
- A JWT will be created as part of secret.

Add `minio` to your helm charts repository:

```bash
helm repo add minio https://operator.min.io/
```

Install the `minio/operator` chart:

```bash
helm install \
  --namespace minio-operator \
  --create-namespace \
  minio-operator minio/operator
```
Follow instructions to retrieve the generated JWT and gain acssess to the MinIO operator Console if needed.

> ℹ️ **Note:** 
> For this example, we've created a user with write permission to gain acsess from the tenants that will be deployed in the cluster and write data into the DB, This can be done in various ways (UI console, CLI etc.)
>
> For more information, see [MinIO user managment](https://docs.min.io/minio/vsphere/tutorials/user-management.html).

### Launching a Standalone Tenant

Download the Helm chart values [file](https://github.com/minio/operator/blob/master/helm/tenant/values.yaml) and edit the following arguments:

- `tenants.pools.server` = `1`
- `tenants.pools.storageClassName` = `default.your-statehub-org-id.statehub.io`


Your `values.yaml` file should resemble:

```yaml
tenants:
  - name: minio1
    image:
      repository: quay.io/minio/minio
      tag: RELEASE.2022-01-08T03-11-54Z
      pullPolicy: IfNotPresent
    namespace: default
    imagePullSecret: { }
    scheduler: { }
    pools:
      ## Servers specifies the number of MinIO Tenant Pods / Servers in this pool.
      ## For standalone mode, supply 1. For distributed mode, supply 4 or more.
      ## Note that the operator does not support upgrading from standalone to distributed mode.
      - servers: 1
        ## volumesPerServer specifies the number of volumes attached per MinIO Tenant Pod / Server.
        volumesPerServer: 4
        ## size specifies the capacity per volume
        size: 10Gi
        ## storageClass specifies the storage class name to be used for this pool
        storageClassName: default.cs-dep.statehub.io
        ## Used to specify a toleration for a pod
...

```

Install the chart with the new Value File:

```
helm install tenants -f value-file.yml minio/tenant
```

follow instructions to acsess the minio console, using the credantials of the User created earlier in this guide.


**At this point, you have a MinIO installation that's replicated between your locations.**
 
 

## Step 4: Generate Test Data (Optional)

Once you accessed the MinIO on your tenant pod you can add data to your buckets in various ways (including through the Minio UI console). More information about adding data to your server [here](http://nm-muzi.com/docs/minio-client-complete-guide.html#ls). Add files or objects as you please.

You now have new data ready to be tested for a switch over between locations using Statehub.

After starting your application, your topology will resemble this:

![active location](assets/images/minio/img2.png)


## Step 5: Failing Over Between Clusters in Different Locations

![failover](assets/images/minio/img5.png)

When you need to switch over your application to another cluster, set the other cluster as the [owner of the state](states.md#state-ownership) and apply the configuration to it as follows:

Remove the tenant helm chart from its original location (optional, so it will be easier for you to fail the application back):

```bash
helm delete tenants 
```
You may also delete the operator helm chart:

```
helm deletee minio-operator -n minio-operator
```

### Start Your Application on the Other Cluster:

1. Choose the cluster on which you want your application and switch to its context

        kubectl config use-context another-cluster

1. Make sure this cluster is the owner of the default state by running the following command:

        statehub set-owner default another-cluster

1. Apply your application configuration (same as step 3):

    ```bash
    helm install \
      --namespace minio-operator \
      --create-namespace \
      minio-operator minio/operator
    ```

    And your tenants:
    ```
    helm install tenants -f value-file.yml minio/tenant
    ```

You now have launched a new MinIO cluster in the secondery location with access to the Global PVC that contains the data written on the original cluster.

![after failover](assets/images/minio/img6.png)

## Conclusion

You have successfully now deployed a MinIO installation that's replicated between multiple locations in real time. Statehub makes it resilient to infrastructure failures at the AZ, region and cloud level. Additional locations can be added at any time, so you'll be free to move your applications to any cloud location in the world.
