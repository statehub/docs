Just like `kubectl` is the most useful tool to manage Kubernetes clusters, the Statehub CLI is used to control your Statehub organizaion. While most operations can be performed with the Statehub Management Console (<a href="https://console.statehub.io" target="_blank">https://console.statehub.io/</a>), some operations require the CLI.

## Getting the Statehub CLI

To install Statehub run the following command on Linux or macOS:

```bash
curl https://get.statehub.io/get-statehub.sh | sh
```

This will:

1. Download the Statehub CLI, and install it in your `$HOME/.statehub/bin`
2. Add `$HOME/.statehub/bin` to your `$PATH`
3. Run `statehub login` to authenticate your CLI installation and generate a token (in case a valid token is not installed on the system)
   <!-- Make sure `statehub login --verify` works and that the installation script supports the functionality -->

If you do not want to your `$PATH` modified, or to perform authentication automatically, download the CLI installer:

```bash
wget https://get.statehub.io/get-statehub.sh
```

Then, you can run it with `-n` to prevent login, or with `--no-modify-path` to prevent `$PATH` modification:

```bash
sh get-statehub.sh -n --no-modify-path
```

## Authenticating the CLI

In order for the Statehub CLI to function, it needs to be authenticated against your Statehub account. By default, the installation process prompts you to perform a login upon successful installation. However, in advanced cases, login can be performed manually in one of the following ways:

* **Perform an interactive login**

    To perform an interactive login, run

        statehub login
  
    The CLI will prompt you with a link to the Statehub Management Console that will generate a token for your CLI installation, which you should copy from your browser, and paste to the CLI prompt.

<!--    To perform an interactive login, only in case no valid token is available to the CLI, run

        statehub login --check-->

* **Generating a token manually and updating the configuration file**

    This process is useful for automated installations, CI/CD pipelines and other unattended processes. It requires you to obtain a token by following the procedure described in [Access Tokens](account.md#access-tokens). Once you have your token, run:

        statehub save-config
    
    Add the following line to the to the configuration file at `~/.statehub/config.toml` (replacing `xxx` with your token):

        token = 'xxx'

    If the configuration file contains a valid token, the installation process will not prompt for an interactive login. Thus, you can put a valid configuration file at `~/.statehub/config.toml` prepopulated with a token, and then install the CLI in automation scenarios.

* **Using an environment variable to authenticate**

    You can use the `SHTOKEN` environment variable to store your token when running the Statehub CLI in ephemeral environments like CI/CD pipelines.

    As with the configuration file, installation will skip interactive login if the `SHTOKEN` environment variable contains a valid token, making this an option running the CLI from within automated processes.

## Updating the CLI

To update the Statehub CLI, either perform the installation procedure outlined in <a href="#getting-the-statehub-cli">Getting the Statehub CLI</a> or run:

```
statehub self-update
```
