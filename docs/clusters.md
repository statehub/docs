The term "Cluster" refers to any Kubernetes cluster you register with Statehub. Once a cluster is registered with your Statehub organization, it will be able to access your Statehub states and volumes.

Statehub allows you to register multiple clusters in various geographic locations and cloud vendors, and to easily move your stateful applications between them.

## Registering a Cluster

Registering clusters is done with the Statehub CLI. During the registration process, the following happens:

1. The cluster's location(s) and type (AKS/EKS/generic/...) are identified. These will indicate which specific permissions and component settings are required for your cluster to work with Statehub.
1. An API token will be created for your cluster so that your cluster can identify itself to the Stathub API, and will be stored in the clusters secrets.
1. Cloud credentials required for the cluster to access the states and volumes will be stored in your cluster's secrets.
1. Statehub components responsible for allowing your cluster to access Statehub and your volumes will be deployed on your cluster using `helm` (can be overridden).

After registration is complete, you can deploy your stateful applications. It might take 2-3 minutes for your cluster to populate the [required configuration](states.md#visibility-and-access) for your applications to work.

To register the cluster interactively run:

```bash
statehub register-cluster
```

Without additional command line arguments, the CLI will prompt you with the some questions. Statehub requires you to provide certain information in order to register a cluster. This information can be provided interactively, or using via command line arguments, however, most arguments have a default value and interactive registration can be skipped by running:

```bash
statehub register-cluster -y [cloud credential arguments. see below]
```

The following is the information you need to provide in order to register a cluster, and the way to supply answers from command line arguments, instead of interactively:

### Select KUBECONFIG context to register

The CLI will show you a list containing your kubeconfig contexts. Choose the one that corresponds with the cluster you want to register. 

**Default value:** the current context

**CLI arguments:** the context can be provided with `-c`.

    statehub register-cluster -c context-name

### Choose K8s cluster name

Since Kubernetes clusters don't have names, but Statehub needs a name to identify your cluster by, you'll be asked to choose a name for the cluster so that you can refer to it when interacting with the Statehub CLI, API and Management Console.

**Default value:** the name of the context

**CLI arguments:** the name is the last argument provided to the command

    statehub register-cluster cluster-name

### Choose K8s Namespace to install Statehub

This is the namespace in which all the Statehub controller and CSI driver pods will run.

**Default value:** `statehub-system`.

**CLI arguments:** the namespace can be provided `--namespace`

    statehub register-cluster --namespace other-namespace

### Run 'helm install' to install Statehub K8s components?

In order for your cluster to work with Statehub, it's necessary to install the Statehub operator, the state controller and the Statehub CSI driver on your cluster. It is recommended to choose to install them at this point.

If you answer no, you'll have to install the components manually later – the exact commands to install the components can be retrieved with `statehub show-cluster` once the cluster is registered.

**Default value:** yes

**CLI arguments:** installation of the components can be skipped with `--skip-helm`

    statehub register-cluster --skip-helm

### Select states to use with this cluster

You can choose the [states](states.md) to which the locations of this cluster should be added. By default, the [default state](states.md#the-default-state) is selected.

**Default value:** `default`

**CLI arguments:** a list of states can be supplied using `--state`

    statehub register-cluster --states state1 --states state2

Alternatively, you can also choose to not extend any of the states to the locations of the cluster you're registering by using `--no-state`, which will result in the cluster being able to access only the states that already span its location

    statehub register-cluster --no-state

### Claim ownership of unowned states

Indicate whether you want this cluster to become the [owner](states.md#state-ownership) of the states selected in the previous step, if any of them are not currently owned by any other cluster.

**Default value:** yes

**CLI arguments:** ownership claim can be skipped with `--no-state-owner`

    statehub register-cluster --no-state-owner

### Provide cloud credentials

Depending on the cloud provider for the cluster you're registering, you will be promped to create and provide a set of cloud credentials for your cluster, so that it would be able to provision the network components necessary for your cluster to connect to the state and consume the volumes.

> **⚠️ Important:**
>
> Statehub does not store your cloud credentials. The credentials you supply are stored **only** in your cluster's secrets.

**Default value:** *none*

#### AWS

You will be prompted to run a CloudFormation template that will create an Access Key ID and Secret Access Key, and will be requested to paste the output of the CloudFormation into the CLI.

**CLI arguments:** *required when `-y` is specified.* Access Key ID and Secret Access Key can be supplied with `--aws-access-key-id` and `--aws-secret-access-key` respectively

    statehub register-cluster --aws-access-key-id AKIXXXXXXXXXXXXXXXXX --aws-secret-access-key xxx

#### Azure

Depending on whether you have the `az` CLI installed, you will be asked to download and run a script locally (in another shell session), or provided a link to CloudShell to run a script there. The script will ask you to specify the subsription in which the cluster you're registering is running, and will output a set of credentials, Client ID, Client Secret, and Tenant ID, which you'll need to paste into the CLI.

**CLI arguments:** *required when `-y` is specified.* Client ID, Client Secret, and Tenant ID can be supplied with `--azure-client-id`, `--azure-client-secret`, and `--azure-tenant-id` respectively

    statehub register-cluster --azure-client-id xxx --azure-client-secret xxx --azure-tenant-id xxx

## Getting Registered Cluster Information

To get a list of all clusters:

* Using the Statehub CLI:

        statehub list-clusters

    Output example:

        ☸            azure-us-east [azure/eastus2]
        ☸              aws-us-west [aws/us-west-2]
        ☸           aws-eu-central [aws/eu-central-1]

    In this example, we see three clusters, `azure-us-east`, `aws-us-west`, and `aws-eu-central`, located in Azure region EastUS2, AWS region us-west-2, and AWS region eu-central-1 respectively.

* Using the Statehub Management Console:

    1. Go to <a href="https://console.statehub.io/clusters/" target="_blank">https://console.statehub.io/clusters/</a>
    
    ![Generating a Token](assets/images/clusters-list.png)

To show detailed cluster information:

* Using the Statehub CLI:

    To show the details of the cluster associated with the current context (assuming the name of the cluster is the same as the name of the context), run:

        statehub show-cluster

    To show the details of a cluster by specifying its name, run:

        statehub show-cluster my-cluster

    Output example:

        Cluster:     aws-us-west
        Id:          7e74b235-3358-4afd-8786-ac44fde47ed5
        Locations:   aws/us-west-2
        Created:     3 days ago
        Modified:    now
        Helm install:
                      helm install statehub-csi-driver --namespace statehub-system --repo https://helm.statehub.io --version 1.0.25 statehub-csi-driver --set provider=eks
                      helm install state-controller --namespace statehub-system --repo https://helm.statehub.io --version 0.0.27 state-controller
                      helm install statehub-controller --namespace statehub-system --repo https://helm.statehub.io --version 0.1.7 statehub-controller
        Visible states:
            default

    The output gives out the following information about the cluster:

    * Name
    * Unique ID in the Statehub system
    * A list of locations where the cluster has nodes
    * When was the cluster registered and modified
    * What `helm` commands and arguments were run or are necessary to install the Statehub components on the cluster
    * A list of states visible to the cluster (states available in the cluster's locations)

&nbsp;

* Using the Statehub Management Console:

    1. Go to https://console.statehub.io/clusters/{cluster-name}

## Unregistering a Cluster

When you no longer want a cluster to have access to your Statehub organization and resources, you can unregister it. This operation will result in:

1. Invalidation of the cluster's API token, so that the cluster is no longer able to communicate with Statehub
2. Disconnection of the state from the cluster, by removing any networking components provisioned in your cloud account
3. Removal of the states' CRDs and the associated storage classes the from the cluster
4. Removal of the Helm charts with the Statehub components from the cluster

To unregister a cluster:

* Using the Statehub CLI:

    Specify the name of the cluster you want to unregister, by running:

        statehub unregister-cluster my-cluster

    If running in a script or an automated process, you can skip confirmation with the `-f` flag:

        statehub unregister-cluster -f my-cluster

* Using the Statehub Management Console:

    1. Go to <a href="https://console.statehub.io/clusters" target="_blank">https://console.statehub.io/clusters</a>
    2. Click on the cluster you want to unregister
    3. Click the "Unregister" button on the bottom of the page and confirm when prompted

## Cluster Locations

Statehub supports clusters that are located in the public cloud (currently, AWS and Azure). In most cases, a cluster will be located in a single location – a specific region of a specific cloud (e.g., `aws/us-west-1`, `azure/eastus2`, etc.) In specific cases, a cluster can be installed in multiple locations, for example when a cluster spans multiple cloud vendors in the same geographical region.

Once a cluster is registred and the Statehub components on the cluster are running, it will report its location(s) to Statehub, so that it could be given access to the states and volumes available there.
