# Statehub

Statehub is a fully managed storage service for Kubernetes. Utilizing the Statehub's Global PersistentVolume, allows its user to move its stateful applications between different Kubernetes clusters immediately, regardless of where they are in the public cloud.

Here is an illustration that shows how a state holding multiple Global PVs is deployed to enable maximum agility and resiliency.

//TODO: insert image

## What Will You Do In this exercise

* Register to statehub.
* Deploy Statehub's Container Storage Interface(CSI) and Operator using the Statehub CLI.
* Deploy a stateful workload that utilizes the Statehub StorageClass.
* Insert data to your newly created database.
* Switch over to a second cluster possibly in another region or cloud.
* Verify your data is in the database after the switch over.

## Assumptions
* You have already provisioned or imported two Kubernetes cluster that are located in either of the supported locations of Statehub's free tier, which are listed [here](https://docs.statehub.io/subfreetier.html#free-tier).
<!--
## Challenges
## Secrets Management
-->
## Step 1: Sign up to Statehub
* Go to: https://statehub.io/start and fill your name,email and organization ID.
* Check your inbox for the invitation email and click on "Accept Invitation".
* Type in a password of your choice and sign-in.

## Step 2: Get the Statehub CLI
* Go to https://get.statehub.io copy the command and paste it in your terminal.
* Open the prompted link and allow CLI access.
* Copy and paste the new token into the prompt from the last step.
* Open a new terminal and verify that the `statehub` command is in your $PATH, if not you can find it under `~/.statehub/bin/statehub`

## Step 2: Setup KUBECONFIG
* Change the context of your Kubeconfig by running:
    ```
    export KUBECONFIG=~/Downloads/
    kubectl config use-context cluster1
## Step 2: Register Your Clusters
Registering clusters is done with the Statehub CLI.

To register the cluster interactively run the `statehub register-cluster` command and follow the instructions promted.

The `register-cluster` subcommand:
1. Creates an API token for your cluster that will be stored in the clusters secrets.
3. Stores the cloud credentials in the cluster's secrets. The secrets will be used later to create a PrivateLink endpoint in the cluster's VPC so its nodes could access the Statehub states and volumes.
4. Deploys the Statehub components responsible for allowing your cluster to access Statehub and your volumes using helm.

## Step 3: Create a Statefulset
* A few minutes after registration is complete(5 minutes approximately, depending on the cluster's location), verify the `default` state StorageClass is deployed, run:
    ```
    kubectl get storageclass
    ```
    The output should contain a storageClass entry similar to this:
    ```
    default.my-org.statehub.io (default)  csi.statehub.io  Delete  WaitForFirstConsumer  false  2s
    ```

* Once the storageClass is created, create a MySQL StatefulSet and Service by applying the following YAML:
    ```
    ---
    apiVersion: v1
    kind: Service
    metadata:
    name: mysql
    spec:
    ports:
    - port: 3306
    selector:
        application: mysql
    clusterIP: None
    ---
    apiVersion: apps/v1
    kind: StatefulSet
    metadata:
    name: db-sts
    spec:
    selector:
        matchLabels:
        application: mysql
    serviceName: mysql
    replicas: 1
    template:
        metadata:
        labels:
            application: mysql
        spec:
        containers:
        - image: mysql:5.6
            name: mysql
            env:
            - name: MYSQL_ROOT_PASSWORD
            value: password
            ports:
            - containerPort: 3306
            name: mysql
            volumeMounts:
            - name: dbdata
            mountPath: /var/lib/mysql
    volumeClaimTemplates:
    - metadata:
        name: dbdata
        spec:
        accessModes: [ "ReadWriteOnce" ]
        resources:
            requests:
            storage: 10Gi
    ```
## Step 4: Insert data to our database
1. Connect to the newly created mysql database:
    ```
    kubectl run -it --rm --image=mysql:5.6 --restart=Never mysql-client -- mysql -h mysql -ppassword
    ```
2. Run the following to create a database, a table and insert a new row:
    ```
    create database example;

    create table example.contacts (name CHAR(20));

    insert into example.contacts (name) values ('Tony');
    ```
3. Verify the insert worked as expected
    ```
    select name from example.contacts;
    ```

## Step 5: Stop the running database
* Delete the Service and StatefulSet created on step 3:
    ```
    kubectl delete svc,sts mysql
    ```
## Step 6: Set Cluster #2 as The Owner of The State
A state can be owned by one cluster at any given time. A cluster that owns the state can:

* Mount the volumes when a consumer pod is deployed.
* Provision new volumes on the state if they do not exist.

Changing the ownership of the state is possible by using the Statehub CLI, Management Console and the REST API.

To change the owner of the state:

    statehub set-owner default cluster2

## Step 7: Change Your Kubeconfig Context
* Change the context of your Kubeconfig by running:
    ```
    kubectl config use-context cluster2
    ```
## Step 8: Create the MySQL StatefulSet
* Using the same YAML from step 3, create the  MySQL StatefulSet and Service.

## Final Step:
1. Connect to your mysql database:
    ```
    kubectl run -it --rm --image=mysql:5.6 --restart=Never mysql-client -- mysql -h mysql -ppassword
2. Verify the insert worked as expected
    ```
    select name from example.contacts;
