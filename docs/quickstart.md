This section will guide you through the steps necessary to set up your Kubernetes clusters to use Statehub for simple application mobility.

## Before You Begin

Before you begin, please familiarize yourself with Statehub concepts such as [Clusters](clusters.md), [States](states.md), [Volumes](volumes.md) and the [Default State](states.md#the-default-state).

**Prerequisites**

The following is necessary to use Statehub with your Kubernetes clusters:

1. A UNIX-compatible command line interface (Linux or Mac terminal, WSL or CygWin for Windows)
2. The `kubectl` command line utility to control your clusters
3. The `helm` command line utility to deploy Helm charts on your clusters

## Step 1: Create a Statehub account

1. Go to [https://statehub.io/start](https://statehub.io/start), and fill your name, email and organization ID.
2. Check your inbox for the invitation email and click on "Accept Invitation".
3. Type in a password of your choice and sign-in.

## Step 2: Set Up the Statehub CLI

**Go to <a href="https://get.statehub.io" target="_blank">https://get.statehub.io</a> to download and install the Statehub CLI.**

Setting up your Kubernetes clusters to use Statehub requires the use of the Statehub CLI. After installation is complete, a link will be displayed to log you in to your Statehub account. Copy the link to your browser and go to the Statehub login page.

If you don't already have a Statehub account, you can create one by clicking a link on the Statehub login page.

Once you've logged in to your Statehub account, you sould be automatically redirected tokens page, and prompted to create a token for your CLI. Click "Yes" to create a token. Copy the token that's been created to the CLI prompt and press Enter.

Your Statehub CLI installation should now be configured for your Statehub account.

## Step 3: Register Your Clusters with Statehub

**Register the clusters using the Statehub CLI.**

The following command will register the cluster associated with your current context:

```bash
statehub register-cluster
```

To register another cluster, switch to its context, and then run register it:

```bash
kubectl config use-context my-cluster
statehub register-cluster
```

In order to use Statehub with your Kubernetes clusters, you must use register them using the Statehub CLI. The Statehub CLI makes use of your Kubernetes configuration contexts to identify your clusters, and is aware of the current context.

To register another cluster, either switch to the appropriate context and run the aforementioned command. You need to register all the clusters you between which you want to be able to move your stateful applications.

This operation will:

1. Make Statehub aware of your clusters
2. Identify your clusters' location(s) and report them to Statehub
3. Generate a cluster token for your cluster and save it in your cluster's secrets, so that your cluster can access the Statehub REST API.
4. Install the Statehub Controller components and the Statehub CSI driver on your cluster
5. Add the cluster's location(s) to the default state, if default state doesn't span this location yet
6. Configure the default state's corresponding storage class (`default.YourStatehubOrgId.statehub.io`) as the default storage class.

Once you've registered all of your clusters, you should be able to start stateful applications and move them between your clusters.

## Step 4: Deploy Stateful Applications

**Apply a configuration containing persistent volumes to your Kubernetes cluster**.

The stateful application manifest is usually described in a stateful set with a volume claim template or a different configuration describing a persistent volume claim. Example volume claim templates section of a stateful set would resemble:

```yaml
  volumeClaimTemplates:
  - metadata:
      name: dbdata
    spec:
      accessModes: [ "ReadWriteOnce" ]
      storageClassName: "default.YourStatehubOrgId.statehub.io"
      resources:
        requests:
          storage: 10Gi
```

> **ℹ️ Note:**
>
> The `storageClassName` is set to the storage class corresponding with the default state (replace `YourStatehubOrgId` with your Statehub organization ID). If you've registered the cluster with default parameters, you can omit the storage class name since your default state storage class should have been set the default for this cluster.

1. Choose the cluster on which you want your application on and switch to its context

        kubectl config use-context my-cluster
        
1. Make sure this cluster is the owner of the default state by running

        statehub set-owner default my-cluster

1. Apply your stateful application configuration

        kubectl apply -f my-stateful-app.yaml


## Step 5: Moving Your Applications Between Clusters

**To move your application to another cluster, set it as the owner of the state and apply the configuration to it**.

1. Optionally, delete the application on the cluster you're moving away from. Don't worry, this will not affect the data:

        kubectl delete -f my-stateful-app.yaml

2. Choose the cluster on which you want your application on and switch to its context

        kubectl config use-context another-cluster

3. Make sure this cluster is the owner of the default state by running

        statehub set-owner default another-cluster

4. Apply your stateful application configuration

        kubectl apply -f my-stateful-app.yaml
