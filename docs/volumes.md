Statehub Volumes are mapped to persistent volume claims (PVCs) made by your applications. If a stateful set is configured to use Statehub for any of the volumes, the requested volumes will be created with the requested size in all of the locations where the state is configured.

The name of the volume in the Statehub CLI, Statehub API and the Statehub Management Console will be the same as its corresponding persistent volume claim, and can be found within the state in which it has been created.

When an application writes to a volume, the changes are synchronized between all of the state's locations in real time.

Thus, creating and configuring Statehub volumes is done by creating PVCs (either directly, or via `volumeClaimTemplates` in stateful sets) while specifying the storage class corresponding with the state in which you want the volume to be created.

> **ℹ️ Note:**
>
> If the cluster has been registered with the default CLI arguments, the default state (`default.YourStatehubOrgId.statehub.io`) can be used by omitting the storage class parameter altogether, since it will have been set as the default storage class.

## Volume's Active Location

The active location of the volume is the location at which the volume is being used (i.e., mounted and connected to a pod). It is the location from which the volume's data is actively being replicated to all other locations. Since most clusters are in located in a single location, so the volume's active location would corresponds to the location of the [owner cluster](states.md#state-ownership). However, in cases in which the [cluster is spread across multiple locations](clusters.md#cluster-locations), the volume's active location corresponds with the the location of the node that runs the pod that uses the volume.

## Creating Volumes

Statehub is designed around Kubernetes' [persistent volume](https://kubernetes.io/docs/concepts/storage/persistent-volumes/) mechanism. As such, preferred and straightforward way to create volumes is by simply adding persistent volume claims or volume claim templates to your Kubernetes workload manifests, and applying them to your clusters, specifying the desired state's corresponding storage class in the claim's `storageClassName` field.

Once a volume is created in a state, it's data will be replicated in real time between the its currently active location (depending on the the [owner cluster](states.md#state-ownership)) to the rest of state's location. When a new location is added to the state, each volume will be replicated to it as well.

While technically supported, manual volume creation is discouraged, since in the most common use-case, stateful sets with persistent volume templates, the name of the PVCs is generated, and in order to operate properly, the name of the volume must match the name of its corresponding PVC.

When applying a manifest to your cluster, a volume will be created whenever a persistent volume claim with a Statehub storage class becomes bound. For example, if a stateful set named `mongo` with 3 replicas has the following volume claim template clause:

```
  volumeClaimTemplates:
  - metadata:
      name: dbdata
    spec:
      accessModes: [ "ReadWriteOnce" ]
      storageClassName: "prod-db.acme.statehub.io"
      resources:
        requests:
          storage: 10Gi
```

This will create a volume for each replica, named `mongo-dbdata-0`, `mongo-dbdata-1`, and `mongo-dbdata-2` on the state named `prod-db`.

You can omit the `storageClassName` field from the volume claim definition if you have a Statehub storage class configured as the cluster's default.

## Deleting Volumes

When a volume is deleted, it is deleted from all state locations, and all of the volume's data will be gone.

Unlike the usual behavior of Kubernetes objects, even when a PVC and the underlying PV are deleted from a Kubernetes cluster, the volume itself and its data is not deleted. This is so since Statehub is a solution that allows you to move application between clusters that are oblivious to one another. Since deleting a configuration on one cluster should not affect on your ability to bring up the same workload on another, and since the configuration and container image reside safely on an outside resource (i.e., a Git repository and a container registry), so does the Statehub volume remain unaffected by deleting volumes from clusters.

Thus, deleting volumes is only possible through the Statehub, and not directly by the managing the Kubernetes cluster.

To delete a volume:

* Using the Statehub CLI:

    Specify the name of the state, and the volume you want to delete from it:

        statehub delete-volume my-state mongo-mysql-0

    You will be asked to confirm the deletion. You can also skip confirmation and force the volume deletion by using `-f`:

        statehub delete-volume my-state mongo-mysql-0 -f
